import socket


class WebApp:
    # when we use this class we should rewrite the parse and process methods in the "child" class
    def parse(self, request):
        """Parse the received request, extracting the relevant information."""

        print("Parse: Not parsing anything")
        return None

    def process(self, parsedRequest):
        """Process the relevant elements of the request.

        Returns the HTTP code for the reply, and an HTML page.
        """

        print("Process: Returning 200 OK")
        return ("200 OK", "<html><body><h1>It works!</h1></body></html>")

    def __init__(self, hostname, port):

        # Create a TCP objet socket and bind it to a port
        mySocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        mySocket.bind((hostname, port))

        # Queue a maximum of 5 TCP connection requests
        mySocket.listen(5)

        # Accept connections, read incoming data, and call
        # parse and process methods (in a loop)

        while True:
            print("Waiting for connections")
            (recvSocket, address) = mySocket.accept()
            print("HTTP request received (going to parse and process):")
            request = recvSocket.recv(2048)
            print(request)
            parsedRequest = self.parse(request.decode('utf8'))
            (returnCode, htmlAnswer) = self.process(parsedRequest)
            print("Answering back...")
            response = "HTTP/1.1 " + returnCode + " \r\n\r\n" \
                       + htmlAnswer + "\r\n"
            recvSocket.send(response.encode('utf8'))
            recvSocket.close()

