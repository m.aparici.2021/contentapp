import webapp

SUB_PAGE2 = """
            <!DOCTYPE html>
            <html>
                <head>
                    <title>Sub2</title>
                    <meta charset="utf-8">
                </head>
                <body>
                    <h1>
                        This is the subpage 2
                    </h1>
                </body>
            </html>
            """

SUB_PAGE1 = """
            <!DOCTYPE html>
            <html>
                <head>
                    <title>Sub1</title>
                    <meta charset="utf-8">
                </head>
                <body>
                    <h1>
                        This is the subpage 1
                    </h1>
                </body>
            </html>
            """

ROOT_PAGE = """
            <!DOCTYPE html>
            <html>
                <head>
                    <title>Main Page</title>
                    <meta charset="utf-8">
                </head>
                <body>
                    <h1>
                        This is the main page:
                    </h1>
                    <form action="main" method="post">
                        <input type="radio" name="page_to_serve" value="p1"> Sub page 1 <br>
                        <input type="radio" name="page_to_serve" value="p2"> Sub page 2 <br>
                        <input type="submit" value="send">
                    </form>
                </body>
            </html>
            """

PAGES_DICT = {"/": ROOT_PAGE, "/p1": SUB_PAGE1, "/p2": SUB_PAGE2}


class ContentApp(webapp.WebApp):

    def parse(self, request):
        parse = request.split('\r\n')
        resource = parse[0].split(" ")[1]
        method = parse[0].split(" ")[0]
        if method == "POST":
            if resource == "/main":
                for i in parse:
                    found = i.find("page_to_serve")
                    if found > -1:
                        resource = "/"+i.split("=")[1]
                        break

        return resource

    def process(self, parsedRequest):
        return ("200 OK", PAGES_DICT[parsedRequest])


if __name__ == "__main__":
    ContentApp("127.0.0.1", 1234)
